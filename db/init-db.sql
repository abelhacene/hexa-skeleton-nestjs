CREATE TABLE IF NOT EXISTS example (
    id bigserial PRIMARY KEY,
    description VARCHAR (128) NOT NULL,
);
