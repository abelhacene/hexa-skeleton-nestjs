Feature: List examples
  In order to see the examples
  As a user
  I want to read the list of available examples

  Scenario: A user wants to see the available examples
    Given a user wants to look at example
    When the user looks for the available examples
    Then he must see all the available examples

# Scenario: A user wants to find example with the test description
#   Given a user wants to see a particular example
#   When the user searches for the example
#   Then he must see the example
