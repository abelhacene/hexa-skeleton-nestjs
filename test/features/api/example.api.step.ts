//TODO response is null in THEN

import { Given, Then, When } from '@cucumber/cucumber';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { expect } from 'chai';
import * as request from 'supertest';
import { AppModule } from '../../../src/app.module';

Given('Writing a example with {string}', function (description: string) {
  this.payload = { description: description };
});

When('I submit the example', async function () {
  const module: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  const app: INestApplication = module.createNestApplication();
  await app.init();

  this.exampleRequest = request(app.getHttpServer())
    .post('/example')
    .send(this.payload);
});

Then('I received a {string} message', function (message: string) {
  this.exampleRequest.end((res) => {
    console.log('ICICII', res);

    expect(res).to.equals(message);
  });
});
