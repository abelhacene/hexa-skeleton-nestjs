import { Given, When, Then, Before } from '@cucumber/cucumber';
import { expect } from 'chai';
import { ExampleDomain } from '../../../src/domain/example/example.domain';
import { ExampleService } from '../../../src/domain/example/example.service';
let inMemoryDb = [];
const findAllExamples = () => inMemoryDb;
const setInMemoryDb = (examples: ExampleDomain[]) => {
  inMemoryDb = examples;
};
// const addAnExample = (example) => inMemoryDb.push(example);
// const getAnExampleById = (id) => inMemoryDb.find(id);

Given('a user wants to look at example', function () {
  setInMemoryDb([
    new ExampleDomain('test'),
    new ExampleDomain('test1'),
    new ExampleDomain('test2'),
  ]);
});
When('the user looks for the available examples', function () {
  this.examples = findAllExamples();
  console.log(this.examples);
});
Then('he must see all the available examples', function () {
  expect(this.examples.length).to.equals(3);
});
