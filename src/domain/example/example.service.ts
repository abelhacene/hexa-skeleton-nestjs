import { Inject, Injectable } from '@nestjs/common';
import { ExampleDomain } from './example.domain';
import { IExampleRepository } from './interface.example.repository';

@Injectable()
export class ExampleService {
  private exampleRepositoryAdapter: IExampleRepository;

  constructor(@Inject('ExampleRepository') exampleAdapter: IExampleRepository) {
    this.exampleRepositoryAdapter = exampleAdapter;
  }

  create(example: ExampleDomain) {
    return this.exampleRepositoryAdapter.save(example);
  }
  getAll() {
    return this.exampleRepositoryAdapter.getAll();
  }
  delete(id: number) {
    return this.exampleRepositoryAdapter.delete(id);
  }
}
