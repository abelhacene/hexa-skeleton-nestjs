import { ExampleDomain } from './example.domain';

export interface IExampleRepository {
  save(example: ExampleDomain): string;
  getAll(): Promise<ExampleDomain[]>;
  delete(id: number): Promise<string>;
}
