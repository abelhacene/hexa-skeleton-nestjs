import { Test, TestingModule } from '@nestjs/testing';
import { ExampleController } from './example.controller';
import { ExampleDomain } from '../domain/example/example.domain';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { ExampleService } from '../domain/example/example.service';
import * as request from 'supertest';

jest.mock('../domain/example/example.service');

describe('ExampleController', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExampleController],
      providers: [ExampleService],
    }).compile();

    app = module.createNestApplication();
    await app.init();
  });

  describe('create', () => {
    const example = new ExampleDomain('description');
    test('should return a status code 201', () => {
      return request(app.getHttpServer())
        .post('/example')
        .send(example)
        .expect(HttpStatus.CREATED);
    });
  });
  describe('getAll', () => {
    test('should return a status code 200', () => {
      return request(app.getHttpServer()).get('/example').expect(HttpStatus.OK);
    });
  });
  test('service is called 2 times', () => {
    // 1 appel dans le provider et 2 appels dans les tests
    expect(ExampleService).toHaveBeenCalledTimes(3);
  });
});
