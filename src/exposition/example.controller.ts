import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { ExampleDomain } from '../domain/example/example.domain';
import { ExampleService } from '../domain/example/example.service';

@Controller('example')
export class ExampleController {
  constructor(private readonly exampleService: ExampleService) {}

  @Post()
  async create(
    @Body() exampleDomain: ExampleDomain,
    @Res() response: Response,
  ): Promise<string | void> {
    const example = await this.exampleService.create(
      new ExampleDomain(exampleDomain.description),
    );
    response.status(HttpStatus.CREATED).send(example);
  }
  @Get()
  async getAll(@Res() response: Response) {
    const examples = await this.exampleService.getAll();
    response.status(HttpStatus.OK).send(examples);
  }
  @Delete(':id')
  async deleteExample(@Res() response: Response, @Param('id') id: number) {
    const message = await this.exampleService.delete(id);
    if (message === 'Job not found') {
      response.status(HttpStatus.NOT_FOUND).send(message);
    }
    response.status(HttpStatus.ACCEPTED).send(message);
  }
}
