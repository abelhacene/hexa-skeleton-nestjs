import { ConfigModule } from '@nestjs/config';
import { ExampleEntity } from '../infrastructure/example.entities';

export default (): ConfigModule => ({
  type: 'postgres',
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT, 10),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [ExampleEntity],
  keepConnectionAlive: true,
  synchronize: true,
});
