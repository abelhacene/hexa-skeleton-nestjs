import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ExampleDomain } from '../domain/example/example.domain';
import { IExampleRepository } from '../domain/example/interface.example.repository';
import { ExampleEntity } from './example.entities';

@Injectable()
export class ExampleAdapter implements IExampleRepository {
  constructor(
    @InjectRepository(ExampleEntity)
    private readonly exampleEntityRepository: Repository<ExampleDomain>,
  ) {}

  public save(example: ExampleDomain): string {
    if (example.description !== '') {
      this.exampleEntityRepository.save(example);
      return 'Success';
    } else {
      return 'Cannot create with empty description';
    }
  }
  public getAll() {
    return this.exampleEntityRepository.find();
  }
  public async delete(id: number): Promise<string> {
    const example = await this.exampleEntityRepository.findOne(id);

    if (example) {
      await this.exampleEntityRepository.delete(id);

      return 'Job was removed';
    }
    return 'Job not found';
  }
}
