import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExampleService } from '../domain/example/example.service';
import { ExampleController } from '../exposition/example.controller';
import { ExampleEntity } from './example.entities';
import { ExampleAdapter } from './example.repository.adapter';

@Module({
  imports: [TypeOrmModule.forFeature([ExampleEntity])],
  exports: [TypeOrmModule],
  providers: [
    ExampleService,
    { provide: 'ExampleRepository', useClass: ExampleAdapter },
  ],
  controllers: [ExampleController],
})
export class ExampleModule {}
