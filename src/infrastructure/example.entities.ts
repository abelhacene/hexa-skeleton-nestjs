import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'exampleTable' })
export class ExampleEntity {
  @PrimaryGeneratedColumn({ name: 'example_id' })
  id!: number;

  @Column({ name: 'description' })
  description!: string;
}
